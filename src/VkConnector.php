<?php

namespace App;

use VK\Client\VKApiClient;

class VkConnector
{
    protected $vkClient;
    protected $wallId;
    protected $token;
    protected $count;

    const MAX_POSTS_PER_REQUEST = 100;

    public function __construct()
    {
        $this->vkClient = new VKApiClient();
        $this->wallId = Registry::get('vk_wall_id');
        $this->token = Registry::get('vk_token');
        $this->count = Registry::get('vk_post_count') ?? 100;
    }

    public function getPosts()
    {
        $posts = [];
        $currentCount = $this->count;
        $offset = 0;
        $ownerType = $this->wallId[0] === '-' || ctype_digit($this->wallId) ?
            'owner_id' : 'domain';
        while ($currentCount > 0) {
            $requestCount = $currentCount > self::MAX_POSTS_PER_REQUEST ?
                self::MAX_POSTS_PER_REQUEST : $currentCount;
            $requestPosts = $this->vkClient
                ->wall()->get($this->token, [
                    $ownerType => $this->wallId,
                    'offset' => $offset,
                    'count' => $requestCount,
                    'filter' => 'owner'
                ]);
            $posts = array_merge($posts, $requestPosts['items']);
            $offset += $requestCount;
            $currentCount -= $requestCount;
            if (count($requestPosts['items']) < self::MAX_POSTS_PER_REQUEST) {
                break;
            }
        }

        return $posts;
    }
}
