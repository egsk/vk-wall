<?php

namespace App;

class Renderer
{
    protected const TEMPLATE_DIR = __DIR__ . '/../templates/';

    public function renderTemplate(string $template, array $params = [])
    {
        extract($params);
        ob_start();
        include(self::TEMPLATE_DIR . $template . '.php');
        $content = ob_get_contents();
        ob_clean();

        return $content;
    }
}
