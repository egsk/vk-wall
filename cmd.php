<?php

use App\Registry;
use App\JoomlaDatabaseManager;

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/../public_html/configuration.php';

init();
$contentManager = new \App\ContentManager();
$contentManager->savePosts();

function init()
{
    $jConfig = new JConfig();
    $dsn = "mysql:host={$jConfig->host};dbname={$jConfig->db}";
    $pdo = new \PDO($dsn, $jConfig->user, $jConfig->password);
    $prefix = $jConfig->dbprefix;
    $db = new JoomlaDatabaseManager($pdo, $prefix);
    Registry::set('JConfig', $jConfig);
    Registry::set('rootDir', __DIR__);
    Registry::set('db', $db);
    Registry::set('pdo',  $pdo);
    Registry::set('vk_token', $jConfig->vk_token);
    Registry::set('vk_post_count', 100);
    Registry::set('vk_image_dir', $jConfig->vk_image_dir);
    Registry::set('vk_image_url', $jConfig->vk_image_url);
    $args = $_SERVER['argv'];
    for ($i = 1; $i < count($args); $i++) {
        switch ($args[$i]) {
            case '--init':
                $db->migrate();
                exit;
            case '--clear':
                $db->clear();
                exit;
            case '--wall':
                $i++;
                if (empty($args[$i])) {
                    throw new InvalidArgumentException();
                }
                Registry::set('vk_wall_id', $args[$i]);
                break;
            case '--count':
                $i++;
                if (empty($args[$i])) {
                    throw new InvalidArgumentException();
                }
                Registry::set('vk_post_count', $args[$i]);
                break;
            case '--token':
                $i++;
                if (empty($args[$i])) {
                    throw new InvalidArgumentException();
                }
                Registry::set('vk_token', $args[$i]);
                break;
        }
    }
}


