<?php

namespace App;

class JoomlaDatabaseManager
{
    protected $pdo;
    protected $tablePrefix;

    public function __construct(\PDO $pdo, string $tablePrefix = '')
    {
        $this->pdo = $pdo;
        $this->tablePrefix = $tablePrefix;
    }

    public function saveContent(array $content)
    {
        $query = $this->pdo->prepare("
            INSERT INTO {$this->tablePrefix}content 
                (
                 title,
                 alias,
                 introtext,
                 catid,
                 vk_post_id,
                 vk_owner_id,
                 created,
                 state,
                 access,
                 `language`,
                 `fulltext`,
                 `images`,
                 `urls`,
                 `attribs`,
                 `metakey`,
                 `metadesc`, 
                 `metadata`
                 ) VALUES (?,?, ?, ?, ?, ?, ?, 1, 1, '*', '', '', '', '', '', '', '')
        ");

        return $query->execute([$content['title'], $content['alias'], $content['introtext'], $content['catid'], $content['vk_post_id'], $content['vk_owner_id'], $content['date']]);
    }

    public function getExtremeSavedPostIdByOwner(string $ownerId, bool $direction): int
    {
        $order = $direction ? 'ASC' : 'DESC';
        $query = $this->pdo->prepare("SELECT vk_post_id FROM {$this->tablePrefix}content WHERE vk_owner_id = ? ORDER BY vk_post_id {$order} LIMIT 1");
        $query->execute([$ownerId]);
        $res = $query->fetch();

        return $res ? $res['vk_post_id'] : 0;
    }

    public function getCatId()
    {
        $id = $this->pdo->query("SELECT id FROM {$this->tablePrefix}categories WHERE alias = 'news'")->fetch();
        if ($id) {
            return $id['id'];
        }
        $id = $this->pdo->query("SELECT id FROM {$this->tablePrefix}categories WHERE extension = 'com_content' LIMIT 1")->fetch();

        return $id ? $id['id'] : 1;
    }

    public function clear()
    {
        $this->pdo->query("DELETE FROM {$this->tablePrefix}content WHERE vk_post_id > 0")->execute();
    }

    public function migrate()
    {
        $this->pdo->query("ALTER TABLE `{$this->tablePrefix}content` 
                ADD COLUMN vk_post_id INT,
                ADD COLUMN vk_owner_id INT");
    }
}
