<?php
/**
 * @var $photos string[]
 */
?>

<?php if (!empty($photos)): ?>
    <div class="vk-post-gallery">
        <?php foreach ($photos as $photo): ?>
            <div class="vk-post-gallery__item">
                <img src="<?= $photo ?>" alt="">
            </div>
        <?php endforeach; ?>
    </div>
<?php endif ?>
