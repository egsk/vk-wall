<?php

namespace App;

class ContentManager
{
    /**
     * @var JoomlaDatabaseManager
     */
    protected $db;
    /**
     * @var VkConnector
     */
    protected $vk;
    /**
     * @var Renderer
     */
    protected $renderer;
    /**
     * @var ContentHelper
     */
    protected $contentHelper;

    /**
     * @var int
     */
    protected $catId;

    public function __construct()
    {
        $this->db = Registry::get('db');
        $this->vk = new VkConnector();
        $this->renderer = new Renderer();
        $this->contentHelper = new ContentHelper();
        $this->catId = $this->db->getCatId();
    }


    public function handlePost(array $post)
    {
        $text = $post['text'];
        $title = $this->contentHelper->createTitle($text);
        $alias = $this->contentHelper->transliterate($title);
        $timestamp = $post['date'];
        $date = (new \DateTime())->setTimestamp($timestamp);
        $paragraphs = explode("\n", $text);
        $paragraphs = array_filter($paragraphs, function (string $paragraph) {
            return mb_strlen($paragraph) > 0;
        });
        $paragraphs = array_values($paragraphs);
        for ($i = 0; $i < count($paragraphs); $i++) {
            $paragraphs[$i] = str_replace("\r", '', $paragraphs[$i]);
            $paragraphs[$i] = str_replace("\n", '', $paragraphs[$i]);
        }
        $textContent = $this->renderer->renderTemplate('content_text', ['paragraphs' => $paragraphs]);
        $attachments = !empty($post['attachments']) ? array_filter($post['attachments'], function($attachment) {
            return $attachment['type'] === 'photo';
        }) : [];
        $photoLinks = [];
        foreach ($attachments as $attachment) {
            $photos = $attachment['photo']['sizes'] ?? null;
            if (!$photos) {
                continue;
            }
            $photo = end($photos);
            $photoLinks[] = $this->handlePhotoUrl($photo['url']);
        }
        $photos = $this->renderer->renderTemplate('content_photos', ['photos' => $photoLinks]);
        $preparedText = $this->renderer->renderTemplate('content', ['text' => $textContent, 'photos' => $photos]);
        $this->db->saveContent([
            'title' => $title,
            'alias' => $alias,
            'introtext' => $preparedText,
            'date' => $date->format('Y-m-d H:i:s'),
            'vk_post_id' => $post['id'],
            'vk_owner_id' => $post['owner_id'],
            'catid' => $this->catId,
        ]);
    }

    public function handlePhotoUrl(string $url): string
    {
        $relativePath = (new \DateTime())->format('Ymd');
        $absolutePath = Registry::get('vk_image_dir');
        $savePath =  $absolutePath . '/' . $relativePath;
        if (!is_dir($savePath)) {
            mkdir($savePath, 0777, true);
        }
        $urlPieces = explode('.', $url);
        $fileExtension = end($urlPieces);
        $fileExtension = explode('?', $fileExtension);
        $fileExtension = reset($fileExtension);
        $imgName = md5($url) . '.' . $fileExtension;
        $img = $savePath . '/' . $imgName;
        copy($url, $img);

        return Registry::get('vk_image_url') . '/' . $relativePath . '/' . $imgName;
    }

    public function savePosts()
    {
        $posts = $this->vk->getPosts();
        if (empty($posts)) {
            return;
        }
        $ownerId = $posts[0]['owner_id'];
        $lastPostId = $this->db->getExtremeSavedPostIdByOwner($ownerId, false);
        $firstPostId = $this->db->getExtremeSavedPostIdByOwner($ownerId, true);
        $posts = array_filter($posts, function ($post) use ($firstPostId, $lastPostId) {
            return (!isset($post['marked_as_ads']) || !$post['marked_as_ads']) &&
                $post['post_type'] === 'post' &&
                empty($post['is_pinned']) &&
                ($post['id'] > $lastPostId || $post['id'] < $firstPostId);
        });
        $posts = array_reverse($posts);
        foreach ($posts as $post) {
            $this->handlePost($post);
            usleep(300);
        }
    }
}
