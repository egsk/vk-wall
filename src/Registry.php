<?php

namespace App;

class Registry
{
    protected static $instances = [];

    private function __construct() {}
    private function __clone() {}

    public static function set(string $alias, $instance)
    {
        static::$instances[$alias] = $instance;
    }

    public static function get(string $alias)
    {
        return static::$instances[$alias] ?? null;
    }
}
